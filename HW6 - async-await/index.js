'use strict';

const btn = document.querySelector('button');

btn.addEventListener('click', getClientAddress);

async function getClientAddress() {
  try {
    const clientIp = await fetch('https://api.ipify.org/?format=json').then(res => res.json());
    const clientLocation = await fetch(`http://ip-api.com/json/${clientIp.ip}?fields=status,continent,country,regionName,city,district`).then(res => res.json());

    renderLocation (clientLocation);
    
  } catch (error) {
    alert(`ERROR: ${error.message}`);
  }
}

function renderLocation ({ continent, country, regionName, city, district }) {

  const div = document.querySelector('div');
  div.insertAdjacentHTML("beforeend",`
      <h2>Я знаю, що ти тут:</h2>
      <ul>
        <li>Континент: <b>${continent}</b></li>
        <li>Країна: <b>${country}</b></li>
        <li>Регіон: <b>${regionName}</b></li>
        <li>Місто: <b>${city}</b></li>
        <li>Район: <b>${district}</b></li>
      </ul>
      `);
}