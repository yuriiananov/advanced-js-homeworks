class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;   
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(value) {
        this._salary = value;
    }
}

console.log(new Programmer('Yurii', 36, 20000, ['JavaScript', 'Python']));
console.log(new Programmer('Dan', 22, 25000, ['JavaScript', 'C#', 'Python']));
console.log(new Programmer('Max', 30, 23000, ['Python', 'C++', 'Java']));