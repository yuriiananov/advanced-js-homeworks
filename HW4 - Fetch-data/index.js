'use strict';

const filmsList = document.createElement('ol');
document.body.prepend(filmsList);

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {
    films.forEach(({ episodeId, name, openingCrawl, characters }) => {
        
      const filmItem = document.createElement('li');
      filmItem.insertAdjacentHTML('afterbegin', `
        <p><b>НОМЕР ЕПІЗОДУ: </b>${episodeId}</p>
        <p><b>НАЗВА ФІЛЬМУ: </b>"${name}"</p>
        <p><b>КОРОТКИЙ ЗМІСТ:</b> ${openingCrawl}</p>
        <p><b>СПИСОК ПЕРСОНАЖІВ ЕПІЗОДУ: </b></p>
        <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      `);

      const characterList = document.createElement('ul');
      const loader = filmItem.querySelector('.lds-spinner');

      const characterRequests = characters.map(charURL =>
        fetch(charURL)
          .then(response => response.json())
      );

      Promise.allSettled(characterRequests)
        .then(results => {
          results.forEach(result => {
            if (result.status === 'fulfilled') {
              const character = result.value;
              const characterItem = document.createElement('li');
              characterItem.textContent = character.name;
              characterList.append(characterItem);
            } else {
              console.error(`ERROR: ${result.reason.message}`);
            }
          });
          loader.style.display = "none";
        })
        .catch(err => alert(`ERROR: ${err.message}`));

      filmItem.append(characterList);
      filmsList.append(filmItem);
    });
  })
  .catch(err => alert(`ERROR: ${err.message}`));