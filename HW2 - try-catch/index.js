'use strict'

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function filterBooks(books) {
    return books.filter((book, index) => {
      try {
        if (!book.author) {
          throw new Error(`Error: Книга з індексом ${index} - відсутня властивість author`);
        }
        if (!book.name) {
          throw new Error(`Error: Книга з індексом ${index} - відсутня властивість name`);
        }
        if (!book.price) {
          throw new Error(`Error: Книга з індексом ${index} - відсутня властивість price`);
        }
        return true;
      } catch (error) {
        console.log(error.message);
        return false;
      }
    });
  }
  
function createBookItem(book) {
    return `
      <li>
        <div class="book">Автор: ${book.author}, Назва книги: ${book.name} - Ціна: ${book.price}</div>
      </li>
    `;
  }
  
function showBooks(books) {
    const rootElem = document.querySelector("#root");
    const booksToShow = filterBooks(books);
    const bookCards = booksToShow.map((book) => createBookItem(book));
    rootElem.innerHTML = `<ul>${bookCards.join("")}</ul>`;
  }
  
  showBooks(books);