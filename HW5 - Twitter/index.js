"use strict";
  
class Card {
  constructor(name, email, title, body, postId) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
    this.postId = postId;
    this.cardElement = document.createElement("div");
  }

  renderCard() {
    this.cardElement.classList.add("card");
    this.cardElement.insertAdjacentHTML(
      "afterbegin",
      `
      <div>${this.name}<img src="./assets/free-icon-verify-9918694.png"><span>${this.email}</span></div>
      <div class="card__content-container">
        <h3>${this.title}</h3>
        <p>${this.body}</p>
      </div>
      <button class="card__btn card__edit" title="РЕДАГУВАТИ"></button>
      <button class="card__btn card__delete" title="ВИДАЛИТИ"></button>
    `
    );

    const deleteBtn = this.cardElement.querySelector(".card__delete");
    deleteBtn.addEventListener("click", this.deleteCard.bind(this));

    const editBtn = this.cardElement.querySelector(".card__edit");
    editBtn.addEventListener("click", this.editCard.bind(this));

    cardsContainer.append(this.cardElement);
  }

  editCard() {
    const editPostTitle = prompt("Редагувати заголовок посту:", this.title);
    const editPostBody = prompt("Редагувати текст посту:", this.body);

    if (editPostTitle !== null && editPostBody !== null) {
      const updatedPost = {
        title: editPostTitle,
        body: editPostBody,
      };

      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedPost),
      })
        .then((res) => {
          if (res.ok) {
            this.title = editPostTitle;
            this.body = editPostBody;
            this.updateCardContent();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  updateCardContent() {
    const titleElement = this.cardElement.querySelector("h3");
    const bodyElement = this.cardElement.querySelector("p");

    if (titleElement && bodyElement) {
      titleElement.textContent = this.title;
      bodyElement.textContent = this.body;
    }
  }

  deleteCard() {
    const userConfirmed = confirm("Ви впевнені, що хочете видалити пост?");
    if (userConfirmed) {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
        method: "DELETE",
      })
        .then((res) => {
          if (res.ok) {
            this.cardElement.remove();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }
}

const cardsContainer = document.querySelector(".cards-container");
const spinner = document.querySelector(".lds-facebook");

fetch("https://ajax.test-danit.com/api/json/users")
  .then((res) => res.json())
  .then((users) => {
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((res) => res.json())
      .then((posts) => {
        posts.forEach((post) => {
          const user = users.find((u) => u.id === post.userId);
          new Card(
            user.name,
            user.email,
            post.title,
            post.body,
            post.id
          ).renderCard();
        });
        spinner.style.display = "none";
      })
      .catch((error) => {
        console.error(error);
      });
  })
  .catch((error) => {
    console.error(error);
  });